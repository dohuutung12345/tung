<div class="container">
    <form method="post" id="editUserForm">
        @csrf
        @method('put')
        <div class="form-div">
            <label>Name</label>
            <input type="text" class="form-control" name="name" value="{{$user->name}}">
        </div>
        <div class="form-div">
            <label>Password</label>
            <input type="password" class="form-control" name="password" value="{{$user->password}}">
        </div>

        <div class="form-div">
            <label>Email</label>
            <input type="text" class="form-control" name="email" value="{{$user->email}}">
        </div>
        <div class="form-div">
            <label>Role</label>
            @foreach($roles as $role)
                <div>
                    <input {{$user->roles->contains('id', $role->id) ? 'checked' : ''}}
                           type="checkbox" name="role[]" value="{{$role->id}}">
                    <label>{{$role->name}}</label>
                </div>
            @endforeach
        </div>
    </form>
    <div class="edit-user-errors"></div>
</div>

