@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="widget">
            <button class="btn btn-primary open-create-user" data-view="members.create">
                Create new user
            </button>
            <div>
                <form  action="{{ route('members.list') }}" method="get" id="searchUserForm" class="form-inline">
                    <select name="role" class="custom-select mr-lg-3">
                        <option value="">All</option>
                        @foreach($roles as $role)
                            <option value="{{$role->id}}">{{$role->name}}</option>
                        @endforeach
                    </select>
                    <input type="text" name="search" class="form-control"
                           placeholder="Enter user name ...">
                </form>
            </div>
        </div>
        <div class="text-center" style="margin: 15px 0px;">
            <h1>User list</h1>
        </div>
        <div class="table-blade">
            @include('members.table')
        </div>
{{--        <div class="table-blade" id="list-user" >--}}
{{--        </div>--}}
    </div>
    {{--delete modal--}}
    <div class="modal fade" id="delUserModal" tabindex="-1" aria-labelledby="delUserLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="delUserLabel">Delete User</h5>
                </div>
                <div class="modal-body">
                    <p>Are you sure delete this user ?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-danger btn-del-user">
                        Delete
                    </button>
                </div>
            </div>
        </div>
    </div>
    {{--end delete modal--}}
    {{--update modal--}}
    <div class="modal fade" id="editUserModal" tabindex="-1" aria-labelledby="editUserLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editUserLabel">Update User</h5>
                </div>
                <div class="modal-body edit-modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-warning btn-edit-user">
                        Update
                    </button>
                </div>
            </div>
        </div>
    </div>
    {{--end update modal--}}
    {{--show modal--}}
    <div class="modal fade" id="showUserModal" tabindex="-1" aria-labelledby="showUserLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="showUserLabel">User Info</h5>
                </div>
                <div class="modal-body show-modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    {{--end show modal--}}
    {{--create modal--}}
    <div class="modal fade" id="createUserModal" tabindex="-1" aria-labelledby="createUserLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="createUserLabel">New User</h5>
                </div>
                <div class="modal-body create-modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-warning btn-create-user">
                        Create
                    </button>
                </div>
            </div>
        </div>
    </div>
    {{--end create modal--}}
@endsection
@section('script')
    <script defer type="text/javascript" src="{{asset('js/user-script.js')}}"></script>
@endsection
