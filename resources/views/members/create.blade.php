<div class="container">
    <form method="post" id="createUserForm">
        @csrf
        <div class="form-div">
            <label>Name</label>
            <input type="text" class="form-control" name="name">
        </div>
        <div class="form-div">
            <label>Email</label>
            <input type="text" class="form-control" name="email">
        </div>
        <div class="form-div">
            <label>Password</label>
            <input type="password" class="form-control" name="password">
        </div>
        <div class="form-div">
            <label>Confirm Password</label>
            <input type="password" class="form-control" name="password_confirmation">
        </div>

        <div class="form-div">
            <label>Role</label>
            @foreach($roles as $role)
                <div>
                    <input type="checkbox" name="role[]" value="{{$role->id}}">
                    <label>{{$role->name}}</label>
                </div>
            @endforeach
        </div>
    </form>
    <div class="create-user-errors"></div>
</div>

