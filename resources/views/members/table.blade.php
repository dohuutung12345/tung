
    <table class="table table-striped text-center">
        <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Role</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody class="user-table-body">

        @foreach($users as $user)
            <tr>
                <td>{{$user->id}}</td>
                <td>{{$user->name}}</td>
                <td>
                    @if($user)
                        @foreach($user->roles as $role)
                            <h5><span class="badge bg-success">{{$role->name}}</span></h5>
                        @endforeach
                    @endif
                </td>
                <td>
                    <button class="btn btn-info open-show-user" data-id="{{$user->id}}"
{{--                            data-action="{{ route('members.search',$user->id) }}"--}}
                            data-view="members.show">
                        Detail
                    </button>
                    <button class="btn btn-warning open-edit-user" data-id="{{$user->id}}"
                            data-view="members.edit">
                        Update
                    </button>
                    <button class="btn btn-danger open-del-user" data-id="{{$user->id}}">
                        Delete
                    </button>
                </td>
            </tr>
        @endforeach

        </tbody>
    </table>
    <div class="pages">{{$users->links()}}</div>




