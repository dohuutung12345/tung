<div class="container">
    <div class="row card">
        <div class="card-header">
            <h2>
                <span class="user_id">{{$user->id}}</span>
                <span>{{$user->name}}</span>
            </h2>
        </div>
        <div class="card-body">
            <div>
                <label>Email:</label>
                <h5>{{$user->email}}</h5>
            </div>
            <div>
                <label>Role:</label>
                @foreach($user->roles as $role)
                    <h3><span class="badge bg-success">{{$role->name}}</span></h3>
                @endforeach
            </div>
        </div>
    </div>
</div>
