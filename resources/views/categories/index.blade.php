@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-sm-8">
            <div class="card">
                <div class="card-body">
                    <button class="btn btn-success" id="refresh" onclick="getAllCategory()">Refresh</button>
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Name</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="card">
                <div class="card-header">
                    <span id="text-add-category">Add New Category</span>
                    <span id="text-update-category">Update Category</span>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <div class="mb-3">
                            <label class="form-label">Name</label>
                            <input type="text" id="name" class="form-control" placeholder="Name..." />
                            <div class="text-danger" id="name-error"></div>
                        </div>
                    </div>
                    <input type="hidden" id="id" />
                    <button id="add-btn" onclick="onAddCategory()" class="btn btn-primary">Add</button>
                    <button id="update-btn" onclick="onUpdateCategory()" class="btn btn-primary">Update</button>
                </div>
            </div>
        </div>
    </div>
    </div>

    {{-- Start Delete Category Model --}}
    <div class="modal" id="delete-category-modal" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Delete role?</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p>U want to delete category?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-dark" data-bs-dismiss="modal">No</button>
                    <button type="button" id="btn-delete" class="btn btn-danger">Delete</button>
                </div>
            </div>
        </div>
    </div>
    {{-- End Delete Category Model --}}
@endsection
<script defer src="{{ asset('js/category-script.js') }}" type="text/javascript"></script>
