@extends('layouts.app')
@section('content')
    <div class="container">
        <h1>Product Manager</h1>
        <a href="{{route('products.create')}}"  class="btn btn-success">Create Products</a>
        {{--        <div>--}}
        {{--            <form class="form-inline col-2 col" action="{{route('products.index')}}" method="GET">--}}
        {{--                @csrf--}}
        {{--                <input class="form-control mr-sm-2 " type="search" placeholder="Search" name="key"--}}
        {{--                       value="{{request(('key'))}}">--}}
        {{--                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>--}}
        {{--            </form>--}}
        {{--        </div>--}}
        <div class="row justify-content-center">
            <div class="card-header">

            </div>
            <table class="table table-striped">
                <tr>
                    <th>id</th>
                    <th>name</th>
                    <th>image</th>
                    <th>option</th>
                </tr>
                @foreach($products as $product)
                    @csrf
                    <tr>
                        <th>{{$product->id}}</th>
                        <th>{{$product->name}}</th>
                        <th>
                            <img src="{{ asset('uploads/products/'.$product->image) }}"  width="50px"alt="">
                        </th>
                        <th>
                            <a class="btn btn-success" href="{{route('products.show',$product->id)}}">view</a>
                            <form action="{{route('products.delete',$product->id)}}" method="POST">
                                @method('DELETE')
                                @csrf
                                <button class="btn btn-danger">delete</button>
                            </form>
                            <a class="btn btn-primary" href="{{route('products.edit',$product->id)}}">edit</a>
                        </th>
                    </tr>
                @endforeach
            </table>
        </div>
        {{ $products ->links() }}
    </div>
@endsection


{{--@section('script')--}}
{{--    <script>--}}
{{--        $(document).ready(function () {--}}
{{--            $(document).on('submit','#addProduct',function (e) {--}}
{{--                e.preventDefault();--}}

{{--                let formData = new FormData($('#addProduct')[0]);--}}
{{--                $.ajax({--}}
{{--                    type:"POST",--}}
{{--                    url: '/products',--}}
{{--                    data:formData,--}}
{{--                    contentType:false,--}}
{{--                    processData:false,--}}
{{--                    success: function (response) {--}}

{{--                    }--}}
{{--                })--}}

{{--            })--}}
{{--        })--}}
{{--    </script>--}}
{{--@endsection--}}
