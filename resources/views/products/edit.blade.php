@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h2>Product Insert</h2> <br/>
                <form action="{{route('products.update',$product->id)}}" method="POST" enctype="multipart/form-data">
                    @method('PUT')
                    @csrf
                    <div class="form-group mb-3">
                        <label for="">Name</label>
                        <input type="text" class="form-control" name="name" value="{{$product->name}}">
                        @error('name')
                            <span class="error text-danger" id="name-error" for="name">{{$message}}</span>
                        @enderror
                    </div>
                    <div class="form-group mb-3">
                        <label for="">Iamge</label>
                        <input type="file" class="form-control" name="image">
                        <img src="{{ asset('uploads/products/'.$product->image) }}"  width="50px"alt="">
                        @error('image')
                        <span class="error text-danger" id="images-error" for="iamge">{{$message}}</span>
                        @enderror
                    </div>
                    <div class="form-group mb-3">
                        <button type="submit" class="btn btn-success">bam thu ma xem</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

