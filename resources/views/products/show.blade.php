@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            @csrf
            <table class="table table-striped">

                <tr>
                    <th>id</th>
                    <th>name</th>
                    <th>Image</th>
                    <th>option</th>
                </tr>
                    <tr>
                        <th>{{$product->id}}</th>
                        <th>{{$product->name}}</th>
                        <th>
                            <img src="{{ asset('uploads/products/'.$product->image) }}"  width="50px"alt="">
                        </th>

                        <th>
                            <form action="{{route('products.delete',$product->id)}}" method="POST">
                                @method('DELETE')
                                @csrf
                                <button class="btn btn-danger">delete</button>
                            </form>
                            <a class="btn btn-primary" href="{{route('products.edit',$product->id)}}">edit</a>
                        </th>
                    </tr>
            </table>
        </div>
    </div>
@endsection
