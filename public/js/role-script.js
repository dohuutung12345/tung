//sua ten role


$('#add-role').show();
$('#add-btn').show();
$('#update-btn').hide();
$('#update-role').hide();


// ---------- Get all role -------------------
function getAllRole()
{
    $('#refresh').hide();
    cleanData();
    $.ajax({
        type: "GET",
        dataType: 'json',
        url: '/roles/getAllRole',
        success: function(response) {
            let xhtml = '';
            $.each(response, (key, value) => {
                xhtml += '<tr>';
                xhtml += '<td>' + value.id + '</td>';
                xhtml += '<td>' + value.name + '</td>';
                xhtml += '<td>';
                xhtml += '<button class="btn btn-primary mr-2" onClick="onEditRole('+ value.id +')" >Detail </button>';
                xhtml += '<button class="btn btn-danger" onClick="onDeleteRole('+ value.id +')">Delete</button>';
                xhtml += '</td>';
                xhtml += '</tr>';
            })
            $('tbody').html(xhtml);
        }
    })
}

getAllRole();

// ---------- Reset data role -------------------
function cleanData()
{
    $('#name').val('');
    const permissions = $('.permission');
    permissions.each((idx, e) => {
        e.checked = false;
    })
}


// ---------- Create role -------------------
function onAddRole()
{
    let name = $('#name').val();
    let permissions = [];
    $('#cb-view').prop('checked') ? permissions.push(1) : null;
    $('#cb-create').prop('checked') ? permissions.push(2) : null;
    $('#cb-update').prop('checked') ? permissions.push(3) : null;
    $('#cb-delete').prop('checked') ? permissions.push(4) : null;

    $.ajax({
        type: 'POST',
        data: { name, permissions },
        url: '/roles/store',
        success: function(data) {
            getAllRole();
        },
        error: function(error) {
            $('#name-error').text(error.responseJSON.errors.name ?? '');
            $('#permission-error').text(error.responseJSON.errors.permissions ?? '');
        }
    });
}


// ---------- Edit role -------------------
function onEditRole(id)
{
    $('#add-role').hide();
    $('#add-btn').hide();
    $('#update-btn').show();
    $('#update-role').show();

    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: `/roles/${id}/edit`,
        success: function(data) {
            console.log(data);
            cleanData();
            $('#id').val(data.id);
            $('#name').val(data.name);

            const permissions = $('.permission');
            permissions.each((idx, e) => {
                let valueCheckboxItem = +e.value;
                if(data.permissions.indexOf(valueCheckboxItem) !== -1) {
                    e.checked = true;
                }
            })
        }
    });
}

// ---------- Update role -------------------
function onUpdateRole()
{
    let id = $('#id').val();
    let name = $('#name').val();

    let permissions = [];
    $('#cb-view').prop('checked') ? permissions.push(1) : null;
    $('#cb-create').prop('checked') ? permissions.push(2) : null;
    $('#cb-update').prop('checked') ? permissions.push(3) : null;
    $('#cb-delete').prop('checked') ? permissions.push(4) : null;

    $.ajax({
        type: 'PUT',
        dataType: 'json',
        data: {id, name, permissions },
        url: `/roles/update/${id}`,
        success: function(response) {
            $('#add-role').show();
            $('#add-btn').show();
            $('#update-btn').hide();
            $('#update-role').hide();
            getAllRole();
            cleanData();
        },
        error: function(error) {
            $('#name-error').text(error.responseJSON.errors.name ?? '');
            $('#permission-error').text(error.responseJSON.errors.permissions ?? '');
        }
    })
}

// ---------- Delete role -------------------
function onDeleteRole(id)
{
    $('#delete-role-modal').modal('show');
    $('#add-role').show();
    $('#add-btn').show();
    $('#update-btn').hide();
    $('#update-role').hide();

    $('#btn-delete').on('click', () => {
        $.ajax({
            type: 'DELETE',
            dataType: 'json',
            url: `/roles/${id}`,
            success: function(response) {
                getAllRole();
                $('#delete-role-modal').modal('hide');
            }
        })
    })
}
