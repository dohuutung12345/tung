(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });// them base.js
    const DELETE_METHOD = 'DELETE',
        PUT_METHOD = 'PUT',
        GET_METHOD = 'GET',
        POST_METHOD = 'POST';
    //es6
    var delUserModal = $('#delUserModal'),
        editUserModal = $('#editUserModal'),
        showUserModal = $('#showUserModal'),
        createUserModal = $('#createUserModal'),
        searchUserForm = $('#searchUserForm'),
        body = $('.table-blade');

    // console.log(2)
    function getListUser(){
        let formSearch = $('#searchUserForm')
        let url = formSearch.data('action')
        let data = formSearch.serialize();
        crudUser(url,data,'GET').done(function(res){
            $("#list-user").replaceWith(res)
        });
    }

    function crudUser(data,url,method) { //calluserAPI
        return $.ajax({
            url: url,
            method: method,
            data: data,
        });
    }
    $(document).on('click', '.open-del-user', function () {//btn-del-user
        let id = $(this).data('id');
        delUserModal.find('.btn-del-user').data('id', id);
        delUserModal.modal('show');
    });

    $(document).on('click', '.btn-del-user', function () {
        let id = $(this).data('id');
        let url = $(this).data('action');
        // url = '/members/' + id + '/delete',
        type = 'DELETE',
            data = '';
        crudUser(id, data, url, DELETE_METHOD).done(function (res) {
            delUserModal.modal('hide');
            body.html(res);
        });
    });

    $(document).on('click', '.open-edit-user', function () {
        let view = $(this).data('view'),
            id = $(this).data('id'),
            url = '/members/fetch',
            type = 'GET',
            data = {view, id};
        crudUser(id, data, url, type).done(function (res) {
            editUserModal.find('.btn-edit-user').data('id', id);
            editUserModal.find('.edit-modal-body').html(res);
            editUserModal.modal('show');

        });
    });

    $(document).on('click', '.btn-edit-user', function () {
        let id = $(this).data('id'),
            data = $('#editUserForm').serialize(),
            url = '/members/' + id + '/edit';
        crudUser(id, data, url, PUT_METHOD).done(function (res) {
            editUserModal.modal('hide');
            body.html(res);
        }).fail(function (res) {
            let errors = res.responseJSON.errors,
                html = '',
                editUserErrors = $('.edit-user-errors');
            editUserErrors.empty();
            $.each(errors, function (key, error) {
                $.each(error, function (sub_key, sub_error) {
                    html += '<div class="alert alert-danger">' + sub_error + '</div>';
                });
            });
            editUserErrors.append(html);
        });
    });

    $(document).on('click', '.open-show-user', function () {
        let view = $(this).data('view'),
            id = $(this).data('id'),
            data = {view, id},
            url = '/members/fetch',
            type = 'GET';
        crudUser(id, data, url, type).done(function (res) {
            showUserModal.find('.show-modal-body').html(res);
            showUserModal.modal('show');
        });
    });

    $(document).on('click', '.open-create-user', function () {
        let view = $(this).data('view'),
            id = null,
            data = {view},
            url = '/members/fetch',
            type = 'GET';
        crudUser(id, data, url, type).done(function (res) {
            createUserModal.find('.create-modal-body').html(res);//getlistuser
            createUserModal.modal('show');
        });
    });

    $(document).on('click', '.btn-create-user', function () {
        let data = $('#createUserForm').serialize(),
            id = null,
            url = '/members/create',//
            type = 'POST';
        crudUser(id, data, url, type).done(function (res) {
            createUserModal.modal('hide');
            body.html(res);
        }).fail(function (res) {
            let errors = res.responseJSON.errors,
                html = '',
                createUserErrors = $('.create-user-errors');
            createUserErrors.empty();
            $.each(errors, function (key, error) {
                $.each(error, function (sub_key, sub_error) {
                    html += '<div class="alert alert-danger">' + sub_error + '</div>';
                });
            });
            createUserErrors.append(html);
        });
    })

    $(document).on('submit', '#searchUserForm', function (e) {
        e.preventDefault();
        let data = searchUserForm.serialize(),
            id = null,
            url = '/members/search',
            type = 'GET';
        crudUser(id, data, url, type).done(function (res) {
            body.html(res);
        });
    });

    $(document).on('click', '.pagination a', function (e) {
        e.preventDefault();
        let page = $(this).attr('href').split('page=')[1],
            data = searchUserForm.serialize() + '&page=' + page,
            id = null,
            url = '/members/search',
            type = 'GET';
        crudUser(id, data, url, type).done(function (res) {
            body.html(res);
        });
    });

})();





