<?php

namespace App\Providers;

use App\Models\User;
use App\Policies\PermissionPolicy;
use App\Policies\UserPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('admin-only', [UserPolicy::class, 'view']);
        Gate::define('view-products', [PermissionPolicy::class, 'view']);
        Gate::define('create-products', [PermissionPolicy::class, 'create']);
        Gate::define('update-products', [PermissionPolicy::class, 'update']);
        Gate::define('delete-products', [PermissionPolicy::class, 'delete']);
    }
}
