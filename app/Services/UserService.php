<?php

namespace App\Services;

use App\Repositories\UserRepository;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class UserService
{

    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function getPaginate($i)
    {
        return $this->userRepository->getPaginate($i);
    }

    public function getById($id)
    {
        return $this->userRepository->getById($id);
    }

    public function store(Request $request)
    {
        $data = $this->_getDataForStoreOrUpdate($request);
        return $this->userRepository->store($data);
    }

    public function update(Request $request, $id)
    {
        $data = $this->_getDataForStoreOrUpdate($request);
        return $this->userRepository->updated($data, $id);
    }

    public function delete($id)
    {
        return $this->userRepository->delete($id);
    }

    public function search(Request $request)
    {
        return $this->userRepository->search($request->all());
    }

    /**
     * @param Request $request
     * @return array
     */
    public function _getDataForStoreOrUpdate(Request $request): array
    {
        $data = $request->all();
        $data['password'] = bcrypt($data['password']);
        return $data;
    }

}
