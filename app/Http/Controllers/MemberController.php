<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Services\RoleService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class MemberController extends Controller
{
    protected $roleService;
    protected $userService;

    public function __construct(RoleService $roleService, UserService $userService)
    {
        $this->roleService = $roleService;
        $this->userService = $userService;
    }

    public function index(Request $request)
    {
//        $users = $this->userService->search($request);
        $roles = $this->roleService->getAll();
        return view('members.index', compact( 'roles'));
    }
    public function list(Request $request)
    {
        $users = $this->userService->search($request);
        return view('members.table', compact('users'))->render();
    }

    public function store(StoreUserRequest $request)
    {
        $this->userService->store($request);
        $users = $this->userService->getPaginate(10);
        return view('members.table', compact('users'))->render();//

    }


    public function update(UpdateUserRequest $request, $id)
    {
        $this->userService->update($request, $id);
        $users = $this->userService->getPaginate(10);
        return view('members.table', compact('users'))->render();//
    }

    public function delete($id)
    {
        $this->userService->delete($id);
        $users = $this->userService->getPaginate(10);
        return view('members.table', compact('users'))->render();//
    }

    public function search(Request $request)
    {
        $users = $this->userService->search($request);
        return view('members.table', compact('users'))->render();
    }

    public function fetch(Request $request)
    {
        $roles = $this->roleService->getAll();
        if ($request->id) {
            $user = $this->userService->getById($request->id);
            return view($request->view, compact('roles', 'user'))->render();
        }
        return view($request->view, compact('roles'))->render();
    }

}
