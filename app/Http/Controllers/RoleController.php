<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateRoleRequest;
use App\Models\Permission;
use App\Models\Role;
use App\Repositories\RoleRepository;
use App\Services\RoleService;

class RoleController extends Controller
{
    protected $roleService;
    public function __construct( RoleService $roleService)
    {
        $this->roleService = $roleService;
    }

    public function index()
    {
        $roles = $this->roleService->getAll();
        return view('roles.index',compact('roles'));
    }

    public function getAllRole()
    {
        $roles = $this->roleService->getAll();
        return $this->viewRender($roles);
    }

    public function show(int $id)
    {
        $role = $this->roleService->getItemById($id);
        return $this->viewRender($role);
    }

    public function store(CreateRoleRequest $request)
    {
        $role = $this->roleService->store($request);
        return $this->viewRender($role);
    }

    public function edit(int $id)
    {
        $roleData = $this->roleService->edit($id);

        return $this->viewRender([
            'id' => $roleData['role']->id,
            'name' => $roleData['role']->name,
            'permissions' => $roleData['permissions']
        ]);
    }

    public function update(CreateRoleRequest $request, int $id)
    {
        $role = $this->roleService->update($request, $id);
        return $this->viewRender($role);
    }

    public function destroy(int $id)
    {
        $role = $this->roleService->delete($id);
        return $this->viewRender($role);
    }
}
