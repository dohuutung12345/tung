<?php

namespace App\Repositories;

abstract class BaseRepository
{
    protected $model;

    abstract public function model();

    public function __construct()
    {
        $this->makeModel();
    }

    public function makeModel()
    {
        $this->model = app($this->model());
    }

    public function getAll()
    {
        return $this->model->all();
    }

    public function getPaginate($i)
    {
        return $this->model->latest('id')->paginate($i);
    }

    public function getById($id)
    {
        return $this->model->findOrFail($id);
    }

    public function getAllPagination(int $length)
    {
        return $this->model->latest('id')->paginate($length);
    }

    public function getItemById(int $id)
    {
        return $this->model->findOrFail($id);
    }

    public function store(array $data)
    {
        return $this->model->create($data);
    }

    public function updated(array $data, $id)
    {
        $obj = $this->model->findOrFail($id);
        return $obj->update($data);
    }

    public function delete($id)
    {
        $obj = $this->getById($id);
        return $obj->delete();
    }

    public function update(array $data)
    {
        return $this->model->update($data);
    }
}
