<?php

namespace App\Repositories;

use App\Models\Role;

class RoleRepository extends BaseRepository
{
    public function model()
    {
        return Role::class;
    }

    public function onAttach($role, $data)
    {
        $role->permissions()->attach($data);
    }

    public function onDetach($role, $data)
    {
        $role->permissions()->detach($data);
    }

    public function onSync($role, $data)
    {
        $role->permissions()->sync($data);
    }

}
