<?php

namespace App\Repositories;

use App\Models\User;

class UserRepository extends BaseRepository
{

    public function model()
    {
        return User::class;
    }

    public function store(array $data){
        $user = $this->model->create($data);
        $user->attachRole($data['role']);
        return $user;
    }

    public function updated(array $data, $id){
        $user = $this->getById($id);
        $user->update($data);
        $user->syncRole($data['role']);
        return $user;
    }

    public function delete($id){
        $user = $this->getById($id);
        $user->delete();
        $user->detachRole();
        return $user;
    }

    public function search(array $data)
    {
        return $this->model
            ->byName($data['search'] ?? null)
            ->byRole($data['role'] ?? null)
            ->latest('id')
            ->paginate($data['perPage'] ?? 10);
    }

    public function getById($id)
    {
        return $this->model->with('roles')->findOrFail($id);
    }

}
