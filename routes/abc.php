<?php


class abc
{
    protected $a;

    /**
     * abc constructor.
     * @param $a
     */
    public function __construct($a)
    {
        $this->a = $a;
    }

    public function echoa(){
        return $this->a;
    }
}

$laya = new abc(1);
echo $laya->echoa();
