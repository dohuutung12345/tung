<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\MemberController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group([
    'prefix' => 'members',
    'middleware' => ['auth', 'role:admin']
], function () {

    Route::post('/create', [UserController::class, 'store'])->name('members.store');

    Route::get('/fetch', [UserController::class, 'fetch'])->name('members.fetch');

    Route::put('/{id}/edit', [UserController::class, 'update'])->name('members.update');

    Route::delete('/{id}/delete', [UserController::class, 'delete'])->name('members.delete');

    Route::get('/', [UserController::class, 'index'])->name('members.index');

    Route::get('/search', [UserController::class, 'search'])->name('members.search');

    Route::get('/list', [UserController::class, 'list'])->name('members.list');

});

Route::group([
    'prefix' => 'roles',
    'middleware' => ['auth', 'role:admin']
], function () {
    Route::get('/', [RoleController::class, 'index'])
        ->name('roles.index');

    Route::get('/getAllRole', [RoleController::class, 'getAllRole'])
        ->name('roles.all');

    Route::get('/create', [RoleController::class, 'create'])
        ->name('roles.create');

    Route::post('/store', [RoleController::class, 'store'])
        ->name('roles.store');

    Route::get('/{id}/edit', [RoleController::class, 'edit'])
        ->name('roles.edit');

    Route::put('/update/{id}', [RoleController::class, 'update'])
        ->name('roles.update');

    Route::delete('/{id}', [RoleController::class, 'destroy'])
        ->name('roles.delete');

    Route::get('/{id}', [RoleController::class, 'show'])
        ->name('roles.show');
});

Route::group([
    'prefix' => 'products',
    'middleware' => ['auth']
], function () {
    Route::get('/', [ProductController::class, 'index'])
        ->name('products.index')
        ->middleware('permission:product.view');

    Route::get('/create', [ProductController::class, 'create'])
        ->name('products.create')
        ->middleware(('permission:product.create'));

    Route::get('/get-products', [ProductController::class, 'getAllProduct'])
        ->middleware('permission:product.view');

    Route::post('/', [ProductController::class, 'store'])
        ->name('products.store')
        ->middleware('permission:product.create');

    Route::get('/{id}', [ProductController::class, 'show'])
        ->name('products.show')
        ->middleware('permission:product.view');

    Route::get('/edit/{id}', [ProductController::class, 'edit'])
        ->name('products.edit')
        ->middleware('permission:product.update');

    Route::put('/{id}', [ProductController::class, 'update'])
        ->name('products.update')
        ->middleware('permission:product.update');

    Route::delete('/{id}', [ProductController::class, 'destroy'])
        ->name('products.delete')
        ->middleware('permission:product.delete');
});

