<?php

namespace Tests\Feature;

use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreateRoleTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_see_create_form_new_role()
    {
        $this->actingAs(User::factory()->create());
        $response = $this->get($this->getCreateViewRoleRoute());
        $response->assertViewIs('roles.create');
        $response->assertSee('Create New Role');
        $response->assertStatus(Response::HTTP_OK);
    }

    /** @test */
    public function authenticated_user_can_create_role()
    {
        $this->actingAs(User::factory()->create());
        $role = Role::factory()->make()->toArray();
        $response = $this->post($this->getStoreRoleRoute(), $role);
        $response->assertRedirect(route('roles.index'));
        $response->assertStatus(Response::HTTP_FOUND);
    }

    public function getCreateViewRoleRoute()
    {
        return route('roles.create');
    }

    public function getStoreRoleRoute()
    {
        return route('roles.store');
    }
}
