<?php

namespace Database\Seeders;

use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'Con Chủ Tịch',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('12345678'),
        ]);
        $role = Role::create([
            'name' => 'admin'
        ]);

        DB::table('user_role')->insert([
            'role_id' => $role->id,
            'user_id' => $user->id
        ]);
        $permissions = Permission::all();
        foreach ($permissions as $permission){
            DB::table('role_permission')->insert([
                ['permission_id' => $permission->id, 'role_id' => $role->id]
            ]);
        }
    }
}
